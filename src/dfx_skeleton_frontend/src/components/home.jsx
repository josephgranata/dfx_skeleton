import React, {useState, useEffect,useRef, Fragment} from 'react';
import { useIntl,FormattedDate, FormattedNumber} from "react-intl";
import { Menu } from 'primereact/menu';
import { Toast } from 'primereact/toast';
import { Card } from 'primereact/card';


import {login,logout,whoami,isAutenticated,getCanisterOwner,canisterTime,getCyclesAvailable,getCyclesBalance} from '../services/dfx_main'

function Home() {
    const [isLogged, setLogged] = useState(false);
    const [canisterOwner, setCanisterOwner] = useState(undefined);
    const [canisterDateTime, setCanisterDateTime] = useState(undefined);
    const [cyclesAvailable, setCyclesAvailable] = useState(undefined);
    const [cyclesBalance, setCyclesBalance] = useState(undefined);
    const intl = useIntl();
    

    useEffect(() => {
        isAutenticated().then((b)=>{setLogged(b)});
        if (isLogged && !canisterOwner) {
            getCanisterOwner().then((o)=>{
                console.log("canisterOwner: " + JSON.stringify(o));
                setCanisterOwner(o);
            });
        }
        if (isLogged && !cyclesAvailable) {
            getCyclesAvailable().then((a)=>{
                console.log("cyclesAvailable " + a);
                setCyclesAvailable(a);
            });
        }
        if (isLogged && !cyclesBalance) {
            getCyclesBalance().then((c)=>{
                console.log("cyclesBalance " + c);
                setCyclesBalance(c);
            });
        }
        canisterTime().then((dt)=>setCanisterDateTime(new Date(dt)));
        
        
    },[isLogged,cyclesAvailable,cyclesBalance])

    const signedIn = () => {
        setLogged(true);
        tSignIn.current.show({severity:'info', summary: information, detail:contentSignIn, sticky: true});
    }

    const tSignIn = useRef(null);
    const tSignOut = useRef(null);
    const tWhoami = useRef(null);
    const information = intl.formatMessage({ id: 'key.home.sign.info',defaultMessage: 'Information'});
    const contentSignIn = intl.formatMessage({ id: 'key.home.sign-in.content',defaultMessage: 'Authentication is successful'});
    const contentSignOut = intl.formatMessage({ id: 'key.home.sign-out.content',defaultMessage: 'The disconnection is successful'});

    const items = [
        {
            label: intl.formatMessage({ id: 'key.home.menu.signin',defaultMessage: 'Sign in '}) + " (II)",
            icon: 'pi pi-sign-in',
            disabled: isLogged,
            command: () => {
                login(signedIn);
            }
        },
        {
            label: intl.formatMessage({ id: 'key.home.menu.signin',defaultMessage: 'Sign in '}) + " (NFID)",
            icon: 'pi pi-sign-in',
            disabled: isLogged,
            command: () => {
                login(signedIn,true);
            }
        },
        {
            label: intl.formatMessage({ id: 'key.home.menu.signout',defaultMessage: 'Sign out'}),
            icon: 'pi pi-sign-out',
            disabled: !isLogged ,
            command: () => {
                logout().then(()=>{
                    setLogged(false);
                    setCanisterOwner(undefined);
                    setCyclesAvailable(undefined);
                    setCyclesBalance(undefined);
                });
                
                tSignIn.current.show({severity:'info', summary: information, detail:contentSignOut, sticky: true});
            }
        },
        {
            label: intl.formatMessage({ id: 'key.home.menu.whoami',defaultMessage: 'Who am I ?'}),
            icon: 'pi pi-user',
            command: () => {
                whoami().then((i)=>{
                    console.log(i.toString());
                    const contentSignOut = intl.formatMessage({ id: 'key.home.whoami.content',defaultMessage: 'My id is {id}'},{id: i});
                    tSignIn.current.show({severity:'info', summary: information, detail:contentSignOut, sticky: true});
                });
            }
        }
    ];

    const CanisterDateTime = () => {
        if (canisterDateTime ) {
            return (
                <Card className="flex flex-grow-0 mr-2" title={intl.formatMessage({ id: 'key.home.canister-time',defaultMessage: 'Canister date and time'})}>
                    <FormattedDate value={canisterDateTime} timeStyle={'medium'} dateStyle={'medium'}/>
                </Card>
            )
        } else {
            return (<Fragment/>)
        }
    };

    const Owner = () => {
        if (canisterOwner && canisterOwner.ok) {
            return (
                <Card className="flex flex-grow-0 mr-2" title={intl.formatMessage({ id: 'key.home.canister-owner',defaultMessage: 'Canister owner'})}>
                    {canisterOwner.ok.toString()}
                </Card>
            )
        } else if (canisterOwner && canisterOwner.err) {
            return (
                <Card className="flex flex-grow-0 mr-2" title={intl.formatMessage({ id: 'key.home.canister-owner',defaultMessage: 'Canister owner'})}>
                    {canisterOwner.err}
                </Card>
            )
        } else {
            return (<Fragment/>)
        }
    };

    const Balance = () => {
        if (cyclesBalance!==undefined) {
            return (
                <Card className="flex flex-grow-0 mr-2" title={intl.formatMessage({ id: 'key.home.canister-balance',defaultMessage: 'Canister cycles balance'})}>
                    <FormattedNumber value={cyclesBalance} />
                </Card>
            )
        } else {
            return (<Fragment/>)
        }
    };

    const Available = () => {
        if (cyclesAvailable!==undefined) {
            return (
                <Card className="flex flex-grow-0 mr-2" title={intl.formatMessage({ id: 'key.home.canister-available',defaultMessage: 'Canister cycles available'})}>
                    <FormattedNumber value={cyclesAvailable} />
                </Card>
            )
        } else {
            return (<Fragment/>)
        }
    };
    
    return (
        <div className="flex flex-grow-1 mt-1 mb-1 ">
            <Toast ref={tSignIn} position="bottom-right" />
            <Toast ref={tSignOut} position="bottom-right" />
            <Toast ref={tWhoami} position="bottom-right" />
            <div className="flex flex-grow-0 surface-card">
                <Menu model={items} />
            </div>
            
            <div className="flex flex-grow-1 surface-ground ml-1">  
                <div className="flex flex-column">
                    <div className="flex flex-row justify-content-center align-items-center m-2">
                        <CanisterDateTime/>
                        <Owner/>
                        <Balance/>
                        <Available/>
                    </div> 
                    
                </div>
            </div> 
        </div>
    );
}

export default Home;