/*global process*/
import React,{useState,useEffect} from 'react';
import { FormattedMessage,FormattedDate} from "react-intl";
import { Tooltip } from 'primereact/tooltip';
import { useIntl} from "react-intl";
import {ping,whoami,isAnonymous,getCanisterOwner} from '../services/dfx_main';

function Footer() {
    const intl = useIntl();

    const [id,setId] = useState(undefined);
    const [canisterOwner,setCanisterOwner] = useState(undefined);
    const [userState,setUserState] = useState(intl.formatMessage({ id: 'key.footer.ping.user.state.disconnected',defaultMessage: 'disconnected'}));
    const [network,setNetwork] = useState(NaN);
    const [networkIconColor,setNetworkIconColor] = useState('Red');
    const [userIconColor,setUserIconColor] = useState('Red');

    useEffect(() => {
        console.debug('Call useEffect from Footer');
        const beginPing=Date.now();
        if (!network){
            ping().then((ts)=>{
                if(!ts){
                    setNetwork(NaN);
                    setNetworkIconColor('Red');
                    console.warn('Disconnected from backend');
                } else {
                    const endPing = Date.now();
                    setNetwork(endPing - beginPing);
                    setNetworkIconColor('Lime');
                }
            });
        }

        whoami().then((r)=>{
            setId(r);
            isAnonymous().then((b)=>{
                if(r && b){
                    setUserState(intl.formatMessage({ id: 'key.footer.ping.user.state.anonymous',defaultMessage: 'anonymous'}));
                    setUserIconColor('Yellow');
                } else if (r && !b) {
                    setUserState(intl.formatMessage({ id: 'key.footer.ping.user.state.authenticated',defaultMessage: 'authenticated with id={id}'},{id:r}));
                    setUserIconColor('Lime');
                } else {
                    setUserState(intl.formatMessage({ id: 'key.footer.ping.user.state.disconnected',defaultMessage: 'disconnected'}));
                    setUserIconColor('Red');
                }
            });

        });
        getCanisterOwner().then((o)=>{
            if(o.err){
                console.debug(o.err);
            } else if (o.ok) {
                setCanisterOwner(o.ok);
                console.log(o.ok);
            }
            
        });
    },[userState,network])

    const handleNetMouseEnter = () => setNetwork(NaN); // Force a new ping from useEffect

    const netMsg = intl.formatMessage({ id: 'key.footer.tooltip.ping',defaultMessage: 'Ping {t} [ms]'},{t:network});
    const userMsg = intl.formatMessage({ id: 'key.footer.tooltip.user',defaultMessage: 'The user is {u}'},{u:userState});
    const isOwner = id && canisterOwner && (id === canisterOwner)

    return (
        <div className="flex flex-row bg-primary">
            <div className='md:hidden flex flex-grow-1'/>
            <div className='hidden sm:flex flex-grow-1 align-items-center ml-2 text-xs'> 
                <span className='ml-2 text-xs'>{process.env.PACKAGE_LICENCE}</span>
                <span className='ml-2 text-xs'>v{process.env.PACKAGE_VERSION}</span>
                <span className='ml-2 text-xs'> (<FormattedDate value={new Date(process.env.BUILD_DATE)} timeStyle='medium' dateStyle='short' />) </span>
                <span className='ml-2 text-xs'>dfx:{process.env.DFX_VERSION}</span>
                <a href='https://gitlab.com/kurdy/dfx_skeleton' target={'_self'} className='ml-2 text-xs'>
                    <FormattedMessage defaultMessage='Source code' id='component.footer.source'/>
                </a>
            </div>
            <Tooltip target=".net" content={netMsg} position="top"/>
            <div className='net flex flex-grow-0 justify-content-center align-items-center m-3' onMouseEnter={()=>handleNetMouseEnter()}>
                <i className="pi pi-sitemap" style={{'color': networkIconColor}}></i>
            </div>
            <Tooltip target=".user" content={userMsg} position="left"/>
            <div className='user flex flex-grow-0 justify-content-center align-items-center m-3'>
                <i className={`pi ${isOwner?'pi-user-plus':'pi-user'}`} style={{'color': userIconColor}}></i>
            </div>
        </div>
    );
}


export default Footer;