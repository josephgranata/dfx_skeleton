import {locale as primeLocale,addLocale as primeAddLocale,updateLocaleOption} from 'primereact/api';

//primefaces locale https://github.com/primefaces/primelocale

async function fetchLocale(lang) {
    if (lang != 'en') {
        fetch('locale/primelocale/'+ lang +'.json').then((r)=>{
            r.json().then((data)=>{
                //console.log(data[lang]);
                primeAddLocale(lang,data[lang]);
                primeLocale(lang);    
                primeLocale(lang);
                updateLocaleOption('dateFormat',indentifyShortDateFormat(),lang);
            });
        });
    }
    const response = await fetch('locale/'+ lang + '.json');
    return await response.json();

}

export async function getMessage() {
    return await fetchLocale(getLocale());
}

export function getLocale() {
    const locale = localStorage.getItem("app.locale") != null ? localStorage.getItem("app.locale") : navigator.language;
    switch (locale.substring(0,2)) {
        case 'fr' : return 'fr';
        case 'de' : return 'de';
        case 'es' : return 'es';
        default : return 'en';
    }
}

export function getDefaultLocale () {
    return 'en';
}

export async function setLocale (locale) {
    localStorage.setItem("app.locale",locale);
}

export async function removeLocale () {
    localStorage.removeItem("app.locale");
}

function indentifyShortDateFormat(){
    const options = { month: "numeric", day: "numeric", year: "numeric" };
    const date = Date.parse("1985-11-23T22:22:22.222Z");
    let formatedDate = new Intl.DateTimeFormat(navigator.language, options).format(date);
    formatedDate=formatedDate.replace("1985","yy");
    formatedDate=formatedDate.replace("11","mm");
    formatedDate=formatedDate.replace("23","dd");
    return formatedDate;
}