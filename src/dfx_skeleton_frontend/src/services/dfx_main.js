/* global process */
import { dfx_skeleton_backend as canister,createActor,canisterId } from "../../../declarations/dfx_skeleton_backend";
import { AuthClient } from "@dfinity/auth-client";

/***
 * Begin of nfid config https://docs.nfid.one/basic-installation
 */

// Your application's name (URI encoded)
const APPLICATION_NAME = encodeURI(process.env.PACKAGE_NAME);

// URL to 37x37px logo of your application (URI encoded)
const APPLICATION_LOGO_URL = encodeURI('https://hdjs4-yyaaa-aaaak-ab5ea-cai.ic0.app/logo37.webp');

const AUTH_PATH = "/authenticate/?applicationName="+APPLICATION_NAME+"&applicationLogo="+APPLICATION_LOGO_URL+"#authorize";

// Replace https://identity.ic0.app with NFID_AUTH_URL
// as the identityProvider for authClient.login({}) 
const NFID_AUTH_URL = "https://nfid.one" + AUTH_PATH;


/***
 * End of nfid config
 */

const ONE_DAY = BigInt(24 * 60 * 60 * 1000 * 1000 * 1000);

export async function ping() {
    let time=undefined;
    const ts = BigInt(Date.now());
    await canister.ping(ts)
        .then((data)=>{
            time = data;
            console.debug("Ping = " + time);
        }).catch((reason)=>{
            console.warn("Couldn't ping() from main: " + reason);
        });
    return time;
}

export async function canisterTime () {
    let dt = undefined;
    await canister.canisterTime()
        .then((tMs)=>{
            dt = Number(tMs);
        }).catch((reason)=>{
            console.warn("Couldn't canisterTime() from main: " + reason);
        });
    return dt;
}

export async function getCanisterOwner() {
    const authClient = await AuthClient.create();
    const identity = await authClient.getIdentity();
    console.debug("identity: " + JSON.stringify(identity));
    const actor = createActor(canisterId, {
        agentOptions: {
        identity,
        },
    });
    console.debug("actor: " + JSON.stringify(actor) + " - " + actor.toString());
    const id = await actor.getCanisterOwner();
    return id;
}

export async function getCyclesBalance() {
    const authClient = await AuthClient.create();
    const identity = await authClient.getIdentity();
    console.debug("identity: " + JSON.stringify(identity));
    const actor = createActor(canisterId, {
        agentOptions: {
        identity,
        },
    });
    console.debug("actor: " + JSON.stringify(actor) + " - " + actor.toString());
    const balance = await actor.getCyclesBalance();
    return BigInt.asUintN(64, balance[0]);
}

export async function getCyclesAvailable() {
    const authClient = await AuthClient.create();
    const identity = await authClient.getIdentity();
    console.debug("identity: " + JSON.stringify(identity));
    const actor = createActor(canisterId, {
        agentOptions: {
        identity,
        },
    });
    console.debug("actor: " + JSON.stringify(actor) + " - " + actor.toString());
    const available = await actor.getCyclesAvailable();
    return BigInt.asUintN(64, available[0]);
}

export async function login(callback,nfid) {
    let url = "https://identity.ic0.app"; 
    if(process.env.LOCAL_II_CANISTER) {
        url = process.env.LOCAL_II_CANISTER;
    }else if (nfid) {
        url = NFID_AUTH_URL;
    }
    console.info(`login url: ${url}`);
    const authClient = await AuthClient.create();
    const fCb = callback ? callback : handleAuthenticated;
    await authClient.login({
        maxTimeToLive: ONE_DAY,
        onSuccess: async () => {
          fCb();
        },
        identityProvider: url
      });

}

export async function logout() {
    const authClient = await AuthClient.create();
    await authClient.logout({
        onSuccess: async () => {
          console.info("Sign out successful");
        },
      });

}

export async function isAutenticated () {
    let isAutenticated = false;
    const authClient = await AuthClient.create();
    isAutenticated = await authClient.isAuthenticated();
    return isAutenticated;
}

export async function whoami() {
    const authClient = await AuthClient.create();
    const identity = await authClient.getIdentity();
    console.debug("identity: " + JSON.stringify(identity));
    const actor = createActor(canisterId, {
        agentOptions: {
        identity,
        },
    });
    console.debug("actor: " + JSON.stringify(actor));
    let id = await actor.whoami();
    return id;
}

export async function isAnonymous() {
    const authClient = await AuthClient.create();
    const identity = await authClient.getIdentity();
    const actor = createActor(canisterId, {
        agentOptions: {
        identity,
        },
    });
    const ano = await actor.isAnonymous();
    return ano;
}

async function handleAuthenticated(){
    console.info('Sign in sucessful');
}