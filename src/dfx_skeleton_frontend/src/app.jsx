import React,{useState,useEffect} from 'react';
import { IntlProvider} from "react-intl";
import {getDefaultLocale,getLocale,getMessage} from "./services/i18n.js";

import { HashRouter as Router, Route, Routes } from 'react-router-dom';
import ThemeSelector from './themes/theme_selector';
import Home from './components/home';
import Header from './components/header';
import Footer from './components/footer';

import PrimeReact from 'primereact/api';
import { ProgressSpinner } from 'primereact/progressspinner';

import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import "@fontsource/roboto";

// Ripple is an optional animation for the supported components such as buttons.
PrimeReact.ripple = true;

// Input fields come in two styles, default is outlined with borders around the field whereas filled
PrimeReact.inputStyle = 'filled';

function App() {

  const [locale,setLocale] = useState('en');
  useEffect(() => {
      console.info(`Locale change: ${locale}`);
      setLocale(getLocale);
      getMessage().then((m)=>setMessages(m)); 
  }, [locale])

  const [messages,setMessages] = useState(undefined);

  const fallback = <div className="flex justify-content-start align-items-center min-h-screen"><ProgressSpinner/></div>;

  const Display = () => {
    if (messages!=undefined) {
      return (
        <IntlProvider messages={messages} locale={locale} defaultLocale={getDefaultLocale()}>
          <ThemeSelector>
            <div className="flex flex-column min-h-screen surface-ground"> 
              <Router>
                <Header/>
                <Routes>
                      <Route exact path="/" element={<Home/>}/>
                    </Routes>
                <Footer/>
              </Router> 
            </div>   
          </ThemeSelector>
        </IntlProvider>
      );
    } else {
      return fallback
    }
  }
  return <Display/>
}

export default App;