import React,{StrictMode} from 'react';
import {createRoot} from 'react-dom/client';
import App from './app';
import ErrorBoundary from './components/error_boundary';

const container = document.getElementById('app');
const root = createRoot(container);

root.render(
  <StrictMode>
    <ErrorBoundary>
      <App />
    </ErrorBoundary>
  </StrictMode>,
);