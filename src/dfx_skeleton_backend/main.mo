import Text "mo:base/Text";
import Principal "mo:base/Principal";
import Array "mo:base/Array";
import Iter "mo:base/Iter";
import Debug "mo:base/Debug";
import Result "mo:base/Result";
import Nat "mo:base/Nat";
import TrieSet "mo:base/TrieSet";
import Time "mo:base/Time";
import ExperimentalCycles "mo:base/ExperimentalCycles";

shared({caller = canisterOwner}) actor class Main(){

  type Result<T,E> = Result.Result<T,E>;

  stable var allowed : [Principal] = []; 

  system func postupgrade() {
      Debug.print ("Call of Main:postupgrade.");
      if (Nat.equal(Iter.size(Array.vals(allowed)),0)) {
        allowed:= Array.make(canisterOwner);
      }
  };

  system func preupgrade() {
      Debug.print ("Call of Main:preupgrade.");
  };

  public query func ping(timestamp : Nat) : async Nat {
    return timestamp;
  };

  public shared (msg) func getCanisterOwner() : async Result<Principal,Text> {
    if (not Principal.isAnonymous(msg.caller)) {
      #ok(canisterOwner);
    } else {
      #err("Shouldn't be a annonymous caller.");
    }
  };

  public shared (msg) func whoami() : async Text {
      Principal.toText(msg.caller);
  };

  public shared (msg) func isAnonymous() : async Bool {
      Principal.isAnonymous(msg.caller);
  };

  public shared (msg) func getAllowed() : async Result<[Text],Text> {
    if(Principal.equal(canisterOwner,msg.caller)){
      #ok(Array.map<Principal,Text>(allowed, func(i) {Principal.toText(i)}));
    } else {
      #err("Error: Only canister owner can call this function.");
    }
  };

  public shared (msg) func allow(id:Text) : async Result<(),Text> {
    if(Principal.equal(canisterOwner,msg.caller)){
      let p : Principal = Principal.fromText(id);
      allowed := TrieSet.toArray<Principal>(
        TrieSet.put<Principal>(
          TrieSet.fromArray<Principal>(allowed,Principal.hash,Principal.equal),p,Principal.hash(p),Principal.equal));
      #ok 
    } else {
      #err("Error: Only canister owner can call this function.");
    }
    
  };

  public query func canisterTime() : async Int {
    Time.now()/1_000_000;
  };

  public shared (msg)  func getCyclesBalance () : async ?Nat {
    if (not Principal.isAnonymous(msg.caller)) {
      return ?ExperimentalCycles.balance()
    } else {
      return null
    }
  };

   public shared (msg)  func getCyclesAvailable () : async ?Nat {
    if (not Principal.isAnonymous(msg.caller)) {
      return ?ExperimentalCycles.available()
    } else {
      return null
    }
  };

};
